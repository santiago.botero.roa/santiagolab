/*
  Tmperature and Relative Humidity on a Sleep Cycle

  This script reads the HDT11 temperature and relative humidity readigs 
  during an interval of around 5 seconds and then the board goes to sleep for another 10 seconds.

  The circuit:
  Arduino Uno
  DHT11 Temperature and Humidty Sensor

  Created March 12, 2021
  By Santiago Botero
  Modifications from the examples listed below as well as required libraries:
  https://github.com/n0m1/Sleep_n0m1
  https://www.circuitbasics.com/how-to-set-up-the-dht11-humidity-sensor-on-an-arduino/


*/


#include <Sleep_n0m1.h>
#include <dht11.h>

#define DHT11PIN 4
dht11 DHT11;

Sleep sleep;
unsigned long sleepTime; //how long you want the arduino to sleep

void setup()
{
  Serial.begin(9600);
  sleepTime = 10000; //set sleep time in ms, max sleep time is 49.7 days, for 15 minutes set to 900000
 
}

void loop()
{
  delay(100); ////delays are just for serial print, without serial they can be removed
  Serial.println("Beggining read");

  for (int i = 0; i <= 5; i++) {  // Run a loop of 5 measurements spread out over 1 second intervals

  int chk = DHT11.read(DHT11PIN);

  Serial.print("Humidity (%): ");
  Serial.println((float)DHT11.humidity, 2);

  Serial.print("Temperature (C): ");
  Serial.println((float)DHT11.temperature, 2);

  delay(1000);
  }
  Serial.print("sleeping for ");
  Serial.println(sleepTime); 
  delay(5000); //delay to allow serial to fully print before sleep, preparation for time needed for LoRa package to be sent
    
  sleep.pwrDownMode(); //set sleep mode
  sleep.sleepDelay(sleepTime); //sleep for: sleepTime

}
